export default {
    state: {
        filters: null
    },
    mutations: {
        setFilters(state,  filters) {
            state.filters =  filters
        },
    },
    actions: {
         loadFilters({commit}) {
            try {
                if (!localStorage.getItem('setting')) {
                    localStorage.setItem('setting', JSON.stringify([
                        'SOCIALS', 'GROUPS'
                    ]))
                }
                let filters = JSON.parse(localStorage.getItem('setting'))
                commit('setFilters', filters)
            } catch (error) {
                commit('setError', error.message)
                throw error
            }
        },
        changeFilters({commit}, filters){
            try {
                localStorage.setItem('setting', JSON.stringify(filters))
                commit('setFilters', filters)
            } catch (error) {
                commit('setError', error.message)
                throw error
            }
        }
    },
    getters: {
        filters(state) {
            return state.filters
        }
    }
}