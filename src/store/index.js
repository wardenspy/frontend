

import Vue from 'vue'
import Vuex from 'vuex'
import friends from './friends'
import user from './user'
import event from './event'
import axios from 'axios'
import shared from './shared'
import community from './community'
import subscription from './subscription'
import login from './login'
import comparison from './comparison'
import onlineStates from './onlineStates'
import filter from './filter'
import traced from './traced'
Vue.use(Vuex)
// const token = '7ea6b6b6-599f-4808-8c65-5aca1a009b6b';
export const HTTP = axios.create({
    baseURL: 'http://pihanya.ru/v1/api/'
});
export default new Vuex.Store({
    modules:{friends, user, event, shared, community, subscription, login, comparison, filter, onlineStates, traced}}
)