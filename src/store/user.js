import {HTTP} from './index'

export default {
    state: {
        stateUser: null
    },
    mutations: {
        setStateUser(state, stateUser) {
            state.stateUser = stateUser
        },
    },
    actions: {
        async loadStateUser({commit}, id) {
            commit('clearError')
            commit('setLoading', true)
            try {
                const user = await HTTP.get('vkdata/users/' + id)
                commit('setStateUser', user.data)
                commit('setLoading', false)
            } catch (error) {
                commit('setStateUser', null)
                commit('setLoading', false)
                commit('setError', 'Запрашиваемого пользователя нету в базе ВК')
                throw error
            }
        }
    },
    getters: {
        stateUser(state) {
            return state.stateUser
        }
    }
}