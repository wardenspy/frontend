import Vue from 'vue'
import {HTTP} from './index'

export default {
    state: {
        token: '',
        status: '',
    },
    mutations: {
        authRequest: (state) => {
            state.status = 'loading';
        },
        authSuccess: (state, token) => {
            state.status = 'success';
            state.token = token;
        },
        authError: (state) => {
            state.status = 'error';
        },
        authLogout: (state) => {
            state.token = '';
        }
    },
    actions: {

        authLogout({commit}) {
                commit('authLogout');
                Vue.cookie.delete('Authorization')
                Vue.cookie.delete('UserVkId')
                delete HTTP.defaults.headers.common['Authorization']
        }
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
        status(state) {
            return state.status
        },
        token(state) {
            return state.token
        }
    }
}
