import {HTTP} from './index'
import loaderEvents from './optimization/loaderEvents'

const count = 15

export default {
    state: {
        socialEvents: [],
        groupEvents: [],
        events: [],
        load: false,
        eventsRTF: 0,
        isEventsLoading: false
    },
    mutations: {
        setSocialEvents(state, socialEvents) {
            state.socialEvents = socialEvents
        },
        setGroupEvents(state, groupEvents) {
            state.groupEvents = groupEvents
        },
        setEvents(state, events) {
            state.events = events
        },
        concatSocialEvents(state, socialEvents) {
            state.socialEvents = state.socialEvents.concat(socialEvents)
        },
        concatGroupEvents(state, groupEvents) {
            state.groupEvents = state.groupEvents.concat(groupEvents)
        },
        concatEvents(state, events) {
            state.events = state.events.concat(events)
        },
        setLoad(state, load) {
            state.load = load
        },
        nullEventsRTF(state) {
            state.eventsRTF = 0
        },
        setIsEventsLoading(state, isEventsLoading) {
            state.isEventsLoading = isEventsLoading
        }
    },
    actions: {
        async loadEvents({commit, state}, id) {
            if (state.isEventsLoading) return
            commit('clearError')
            commit('setLoad', false)
            commit('isEventsLoading', true)
            try {
                let socialEvents = []
                let groupEvents = []
                let events = await HTTP.get('events/' + id + '?happenedFrom=' + state.eventsRTF + '&count=' + count)
                events = events.data.events
                if( events.length !== 0) {
                    console.log(state.eventsRTF)
                    state.eventsRTF = events[events.length - 1].happenedDate + 1
                    events.forEach((event) => {
                        if (event.serviceType === 'GROUPS') {
                            groupEvents.push(event)
                        } else if (event.serviceType === 'SOCIALS') {
                            socialEvents.push(event)
                        }
                    })
                    loaderEvents(events).then(() => {
                        commit('concatSocialEvents', socialEvents)
                        commit('concatGroupEvents', groupEvents)
                        commit('concatEvents', events)
                        commit('setLoad', true)
                    })
                }
                else{
                    commit('setLoad', true)
                }
            } catch
                (error) {
                commit('setLoad', true)
                commit('setError', error.message)
                throw error
            } finally {
                commit('isEventsLoading', false)
            }
        }
    },
    getters: {
        load(state) {
            return state.load
        },
        socialEvents(state) {
            return state.socialEvents
        },
        groupEvents(state) {
            return state.groupEvents
        },
        events(state) {
            return state.events
        }
    }
}