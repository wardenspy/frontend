import {HTTP} from "./index";

export default {
    state: {
        friends: []
    },
    mutations: {
        setFriends(state, friends) {
            state.friends = friends
        },
        pushFriends(state, friends) {
            state.friends = state.friends.concat(friends)
        },
    },
    actions: {
        async loadFriends({commit}, identifiers) {
            try {
                let friends = await HTTP.post('vkdata/users', identifiers)
                friends = friends.data
                if(friends.length === 1){
                    commit('pushFriends', friends[0])
                }
                else if(friends.length > 1){
                    commit('pushFriends', friends)
                }
            } catch (error) {
                commit('setError', error.message)
                throw error
            }
        }
    },
    getters: {
        friends(state) {
            return state.friends
        },
        friendById: state => id => {
            return state.friends.find(friend => friend.vkId === id)
        },
    }
}