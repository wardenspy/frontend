import {HTTP} from './index'
import loaderComparison from './optimization/loaderComparison'
export default {
    state: {
        comparisonCommunities: null,
        comparisonFriends: null
    },
    mutations: {
        setComparisonCommunities(state, comparisonCommunities) {
            state.comparisonCommunities = comparisonCommunities
        },
        setComparisonFriends(state, comparisonFriends) {
            state.comparisonFriends = comparisonFriends
        }
    },
    actions: {
        async common({commit}, [first, second]) {
            commit('clearError')
            commit('setLoading', true)
            try {
                let communities = await HTTP.get('/functions/authorized/common/groups?id1=' + first + '&id2=' + second)
                let friends = await HTTP.get('/functions/authorized/common/friends?id1=' + first + '&id2=' + second)
                communities = communities.data
                friends = friends.data
                loaderComparison(communities, friends).then(() => {
                    commit('setComparisonCommunities', communities)
                    commit('setComparisonFriends', friends)
                    commit('setLoading', false)
                })
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },
    },
    getters: {
        comparisonCommunities(state){
            return state.comparisonCommunities
        },
        comparisonFriends(state){
            return state.comparisonFriends
        },
    }
}