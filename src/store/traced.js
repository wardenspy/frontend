import {HTTP} from './index'
import loaderUsers from './optimization/loaderUsers'

export default {
    state: {
        traced: []
    },
    mutations: {
        setTraced(state, traced) {
            state.traced = traced
        },
    },
    actions: {
        async loadTraced({commit}) {
            commit('clearError')
            commit('setLoading', true)
            try {
                let traced = await HTTP.get('services?enabled=true')
                traced = traced.data.services
                let objectIdUsers = {};
                for (let i = 0; i < traced.length; i++) {
                    let id = traced[i].trackedVkId
                    objectIdUsers[id] = true
                }
                let idUsers = Object.keys(objectIdUsers).map(function (id) {
                    return Number(id);
                });
                loaderUsers(idUsers).then(() => {

                    commit('setTraced', idUsers)
                    commit('setLoading', false)
                })
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        }
    },
    getters: {
        traced(state) {
            return state.traced
        }
    }
}