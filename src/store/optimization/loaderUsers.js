import store from '..'

export default function loaderUsers(idUsers) {
    return new Promise((resolve, reject) => {
        let massU = []
        idUsers.forEach((id) => {
                let com = store.getters.friendById(id)
                if (!com) {
                    massU.push(id)
                }
            }
        );
        if (massU.length !== 0) {
            store.dispatch('loadFriends', massU).then(() => {
                resolve()
            })
        } else {
            resolve()
        }
    });

}