import store from '..'

export default function loaderComparison(communities, friends) {
    return new Promise((resolve, reject) => {
        let massCommunities = []
        let massFriends = []
        communities.forEach((community) =>{
            let communityId = store.getters.communityById(community)
            if(!communityId){
                massCommunities.push(community)
            }
        })
        friends.forEach((friend) =>{
            let friendId = store.getters.friendById(friend)
            if(!friendId){
                massFriends.push(friend)
            }
        })
        if (massCommunities.length === 0 && massFriends.length === 0) {
            resolve()
        } else if (massCommunities.length !== 0 && massFriends.length === 0) {
            store.dispatch('loadCommunities', massCommunities).then(() => {
                resolve()
            })
        } else if (massCommunities.length === 0 && massFriends.length !== 0) {
            store.dispatch('loadFriends', massFriends).then(() => {
                resolve()
            })
        } else {
            store.dispatch('loadCommunities', massCommunities).then(() => {
                store.dispatch('loadFriends', massFriends).then(() => {
                    resolve()
                })
            })
        }
    });
}