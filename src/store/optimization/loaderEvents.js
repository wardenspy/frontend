import store from '..'

export default function loaderEvents(events) {
    return new Promise((resolve, reject) => {
        let massF = []
        let massC = []
        let ofF = store.getters.friendById(events[0].ofVkId)
        if (!ofF) {
            massF.push(events[0].ofVkId)
        }
        events.forEach((event) => {
                if (event.serviceType === "SOCIALS") {
                    let idF = JSON.parse(event.jsonData).userVkId
                    let com = store.getters.friendById(idF)
                    if (!com) {
                        massF.push(idF)
                    }
                } else if
                (event.serviceType === "GROUPS") {
                    let idC = JSON.parse(event.jsonData).communityId
                    let com = store.getters.communityById(idC)
                    if (!com) {
                        massC.push(idC)
                    }
                }
            }
        );
        if (massC.length === 0 && massF.length === 0) {
            resolve()
        } else if (massC.length !== 0 && massF.length === 0) {
            store.dispatch('loadCommunities', massC).then(() => {
                resolve()
            })
        } else if (massC.length === 0 && massF.length !== 0) {
            store.dispatch('loadFriends', massF).then(() => {
                resolve()
            })
        } else {
            store.dispatch('loadCommunities', massC).then(() => {
                store.dispatch('loadFriends', massF).then(() => {
                    resolve()
                })
            })
        }
    });

}

