import {HTTP} from './index'
const count = 30
export default {
    state: {
        onlineStates: [],
        onlineStatesRTF: Number.MAX_SAFE_INTEGER,
        isOnlineLoading: false
    },
    mutations: {
        setOnlineStates(state, onlineStates) {
            state.onlineStates = onlineStates
        },
        concatOnlineStates(state, onlineStates) {
            state.onlineStates = state.onlineStates.concat(onlineStates)
        },
        setOnlineStatesRTF(state, onlineStatesRTF){
            state.onlineStatesRTF = onlineStatesRTF
        },
        setIsOnlineLoading(state, isOnlineLoading) {
            state.isOnlineLoading = isOnlineLoading
        }
    },
    actions: {
        async loadOnlineStates({commit, state}, id) {
            if (state.isOnlineLoading) return
            commit('setIsOnlineLoading', true)
            commit('clearError')
            try {
                let onlineStates = await HTTP.get('dump/online/' + id + '?count=' + count + '&recordTimeTo=' + state.onlineStatesRTF)
                onlineStates = onlineStates.data
                if(onlineStates.count !== 0){
                    state.onlineStatesRTF = onlineStates.states[onlineStates.count - 1].recordDate - 1
                    commit('concatOnlineStates', onlineStates.states)
                }
            } catch (error) {
                commit('setError', error.message)
                throw error
            } finally {
                commit('setIsOnlineLoading', false)
            }
        }
    },
    getters: {
        onlineStates(state) {
            return state.onlineStates
        },
        isOnlineLoading(state) {
            return state.isOnlineLoading
        }
    }
}