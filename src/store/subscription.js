import {HTTP} from './index'

export default {
    state: {
        active: false
    },
    mutations: {
        setActive(state, active) {
            state.active = active
        }
    },
    actions: {
        async loadActive({commit}, id) {
            try {
                let active = await HTTP.get('services?vkId=' + id + '&types=GROUPS,SOCIALS')
                active = active.data
                if(active.count === 2){
                    commit('setActive', true)
                }
                else{
                    commit('setActive', false)
                }
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },
        async subscribeUser({commit}, id) {
            try {
                const active = await HTTP.post('services/' + id, {})
                if(active.status === 200){
                    commit('setLoading', false)
                }
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        },
        async unSubscribeUser({commit}, id) {
            try {
                const active = await HTTP.delete('services/' + id)
                if(active.status === 200){
                    commit('setLoading', false)
                }
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        }


    },
    getters: {
        active(state) {
            return state.active
        }
    }
}