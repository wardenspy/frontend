import {HTTP} from './index'

export default {
    state: {
        communities: []
    },
    mutations: {
        pushCommunities(state, communities) {
                state.communities = state.communities.concat(communities)
        },
        setCommunities(state, communities) {
            state.communities = communities
        }
    },
    actions: {
        async loadCommunities({commit}, identifiers) {
            try {
                let communities = await HTTP.post('vkdata/groups', identifiers)
                 communities = communities.data
                if(communities.length === 1){
                    commit('pushCommunities', communities[0])
                }
                else if(communities.length > 1){
                    commit('pushCommunities', communities)
                }
            } catch (error) {
                commit('setLoading', false)
                commit('setError', error.message)
                throw error
            }
        }

    },
    getters: {
        communityById: state => id => {
            return state.communities.find(community => community.vkId === id)
        },
        communities(state) {
            return state.communities
        }
    }
}