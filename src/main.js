import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import ListImage from './components/img/ListImage'
import CardImage from './components/img/CardImage'
import Loader from './components/Loader'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAndroid, faApple, faWindows } from '@fortawesome/free-brands-svg-icons'
import { faDesktop, faMobileAlt } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faAndroid)
library.add(faDesktop)
library.add(faApple)
library.add(faWindows)
library.add(faMobileAlt)
Vue.component('font-awesome-icon', FontAwesomeIcon)

import Vuetify from 'vuetify'

var VueCookie = require('vue-cookie')


export const eventEmitter = new Vue()
Vue.component('listAvatar', ListImage)
Vue.component('cardAvatar', CardImage)
Vue.component('loader', Loader)
Vue.use(VueCookie)

Vue.config.productionTip = false
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

