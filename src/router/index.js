import Vue from 'vue'
import Router from 'vue-router'
import ErrorPage from '../components/error/ErrorPage'
import User from '../components/user/User'
import FriendList from '../components/FriendList'
import Comparison from '../components/comparison/ComparisonTile'
import store, {HTTP} from '../store'
import OnlineStateList from "../components/online/OnlineStateList"
import Traced from "../components/traced/Traced"
import Settings from "../components/Settings";

Vue.use(Router)

const AuthGuard = (to, from, next) => {
    const data = {
        "vkId": Vue.cookie.get('UserVkId'),
        "token": Vue.cookie.get('Authorization')
    }
    HTTP.post('auth/check', data).then((check) => {
        if (check.data === 'SUCCESS' && store.getters.isAuthenticated && Vue.cookie.get('Authorization')) {
            next()
        } else {
            Vue.cookie.delete('Authorization')
            Vue.cookie.delete('UserVkId')
            delete HTTP.defaults.headers.common['Authorization']
            next('/error?typeError=' + check.data)
        }
    });

}

export default new Router({
    routes: [
        {
            path: '/friends',
            name: 'FriendList',
            component: FriendList,
            beforeEnter: AuthGuard
        },
        {
            path: '/compare',
            name: 'Comparison',
            component: Comparison,
            beforeEnter: AuthGuard
        },
        {
            path: '/tracked',
            name: 'Tracked',
            component: Traced,
            beforeEnter: AuthGuard
        },
        {
            path: '/o/:id',
            name: 'Online',
            component: OnlineStateList,
            props: true,
            beforeEnter: AuthGuard
        },
        {
            path: '/settings',
            name: 'Settings',
            component: Settings,
            beforeEnter: AuthGuard
        },
        {
            path: '/error',
            name: 'ErrorPage',
            component: ErrorPage,
            props: (route) => ({ typeError: route.query.typeError })
        },
        {
            path: '/:id',
            name: 'User',
            component: User,
            props: true,
            beforeEnter: AuthGuard
        },


    ],
    mode: 'history'
})